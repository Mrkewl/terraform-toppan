resource "aws_subnet" "alb_public_subnet" {
  vpc_id                  = aws_vpc.jazz_toppan_vpc.id
  cidr_block              = var.alb_public_subnet
  map_public_ip_on_launch = true
  availability_zone = var.alb_public_subnet_availability_zone
  tags = {
    "Name" = var.alb_public_subnet_name
  }

}
// put this in tf vars later
variable "alb_public_subnet" {}
alb_public_subnet = "100.0.100.0/24"

variable "alb_public_subnet_availability_zone" {}
alb_public_subnet_availability_zone = "us-east-1a"


variable "alb_public_subnet_name" {}
alb_public_subnet_name = "alb"

resource "aws_subnet" "serverfleet_private_subnet" {
  vpc_id                  = aws_vpc.jazz_toppan_vpc.id
  cidr_block              = var.serverfleet_private_subnet
  map_public_ip_on_launch = false
  availability_zone = var.serverfleet_private_subnet_availability_zone

  tags = {
    "Name" = var.frontend_private_subnet_name
  }
}

variable "serverfleet_private_subnet" {}
serverfleet_private_subnet = "100.0.110.0/24"

variable "serverfleet_private_subnet_availability_zone" {}
serverfleet_private_subnet_availability_zone = "us-east-1b"
