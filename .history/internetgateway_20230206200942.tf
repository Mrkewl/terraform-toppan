#* Elastic IP

resource "aws_eip" "nat_elastic_ip" {
  vpc = true
  tags = {
    "Name" = var.elastic_ip_name
  }
}

variable "elastic_ip_name" {}
elastic_ip_name = "elastic_ip"