#?#####################?#####################?#####################?####################
#* MAIN
#?#####################?#####################?#####################?####################

variable "access_key" {}

variable "secret_key" {}

variable "region" {}


#?#####################?#####################?#####################?####################
#* VPC
#?#####################?#####################?#####################?####################

variable "main_vpc_name" {}

variable "cidr_block" {}


#?#####################?#####################?#####################?####################
#* Subnet
#?#####################?#####################?#####################?####################
#* ALB
variable "alb_public_subnet" {}

variable "alb_public_subnet_availability_zone" {}

variable "alb_public_subnet_name" {}

#* serverfleet subnet
variable "serverfleet_private_subnet" {}

variable "serverfleet_private_subnet_availability_zone" {}

variable "serverfleet_private_subnet_name" {}


#?#####################?#####################?#####################?####################
#* SSH Key
#?#####################?#####################?#####################?####################
variable "ssh_public_key" {}


#?#####################?#####################?#####################?####################
#* Internet Gateway
#?#####################?#####################?#####################?####################
#* Elastic IP

resource "aws_eip" "nat_elastic_ip" {
  vpc = true
  tags = {
    "Name" = var.elastic_ip_name
  }
}

variable "elastic_ip_name" {}
elastic_ip_name = "elastic_ip"


#* INTERNET GATEWAY FOR PUBLIC SUBNET
resource "aws_internet_gateway" "alb_igw" {
  vpc_id = aws_vpc.jazz_toppan_vpc.id
  tags = {
    "NAME" = var.alb_igw_name
  }
}

variable "alb_igw_name" {}
alb_igw_name = "alb_igw"



#* Network Address Translation
resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat_elastic_ip.id
  subnet_id     = aws_subnet.alb_public_subnet.id
  depends_on = [
    # aws_eip.nat_elastic_ip,
    aws_internet_gateway.alb_igw
  ]
}

