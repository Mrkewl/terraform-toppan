#* Elastic IP

resource "aws_eip" "nat_elastic_ip" {
  vpc = true
  tags = {
    "Name" = var.elastic_ip_name
  }
}

variable "elastic_ip_name" {}
elastic_ip_name = "elastic_ip"

#* INTERNET GATEWAY FOR PUBLIC SUBNET
resource "aws_internet_gateway" "dmz_igw" {
  vpc_id = aws_vpc.jazz_capstone.id
  tags = {
    "NAME" = var.dmz_igw_name
  }
}