

resource "aws_iam_policy" "ec2_role" {
  name = "ec2 role"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:*",
          "s3-object-lambda:*"
        ],
        "Resource" : "*"
      }
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}
