resource "aws_security_group" "fleet_private_sec_grp" {
  vpc_id = aws_vpc.jazz_toppan_vpc.id
  name   = "fleet-sec-grp"
#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     security_groups = [aws_security_group.jazz_bastion_sec_grp.id]
#     # cidr_blocks = ["0.0.0.0/0"]
#     # cidr_blocks = [var.my_public_ip]
#   }
    ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = [var.my_public_ip]
  }
      ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = [var.my_public_ip]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Name" = "Jazz-frontend-Security-Group"

  }

}