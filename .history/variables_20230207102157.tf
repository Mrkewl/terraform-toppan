#* MAIN

variable "access_key" {}

variable "secret_key" {}

variable "region" {}

#* VPC

variable "main_vpc_name" {}

variable "cidr_block" {}

#* Subnet
variable "alb_public_subnet" {}

variable "alb_public_subnet_availability_zone" {}