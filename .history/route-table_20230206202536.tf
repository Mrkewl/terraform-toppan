#? ROUTE TABLE ALB

resource "aws_route_table" "alb_route_table" {
  vpc_id = aws_vpc.jazz_toppan_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.alb_igw.id
  }
  tags = {
    "Name" = var.dmz_route_table_name
  }
}

variable "alb_route_table_name" {}
dmz_route_table_name = "alb_route_table"



#? ROUTE TABLE Fleet

resource "aws_route_table" "fleet_route_table" {
  vpc_id = aws_vpc.jazz_toppan_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ngw
  }
  tags = {
    "Name" = var.fleet_route_table_name
  }
}

variable "fleet_route_table_name" {}
fleet_route_table_name = "fleet_route_table"