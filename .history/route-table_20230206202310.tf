#? ROUTE TABLE ALB

resource "aws_route_table" "dmz_route_table" {
  vpc_id = aws_vpc.jazz_toppan_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.alb_igw.id
  }
  tags = {
    "Name" = var.dmz_route_table_name
  }
}