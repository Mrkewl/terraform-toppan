resource "aws_key_pair" "jazz" {
  key_name   = "jazz"
  public_key = file(var.ssh_public_key)

}

#?#####################?#####################?#####################?####################
#* autoscaling grp
#?#####################?#####################?#####################?####################

resource "aws_launch_configuration" "fleet_a_launchbeforeconfig" {
  name_prefix     = "Fleet A"

  image_id        = "ami-0753e0e42b20e96e3"
  instance_type   = "t2.micro"
  user_data       = file("./shell.sh")
  security_groups = [aws_security_group.fleet_private_sec_grp.id]

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "terramino" {
  min_size             = 1
  max_size             = 3
  desired_capacity     = 1
  launch_configuration = aws_launch_configuration.terramino.name
  vpc_zone_identifier  = module.vpc.public_subnets
}





# #* fleet_app
resource "aws_instance" "fleet_app" {
  ami                         = "ami-0753e0e42b20e96e3"
  count                       = 1
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.serverfleet_private_subnet.id
  vpc_security_group_ids      = [aws_security_group.fleet_private_sec_grp.id]
  associate_public_ip_address = false
  key_name                    = aws_key_pair.jazz.key_name
  user_data                   = file("./shell.sh")
  tags = {
    "Name" = "Jazz-fleet"
  }
}

#* test on alb subnet
resource "aws_instance" "bastion" {
  ami                         = "ami-0753e0e42b20e96e3"
  count                       = 1
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.alb_public_subnet.id
  vpc_security_group_ids      = [aws_security_group.jazz_elastic_lb_sec_grp.id]
  associate_public_ip_address = true
  key_name                    = aws_key_pair.jazz.key_name
  user_data                   = file("./shell.sh")
  tags = {
    "Name" = "Jazz-bastion"
  }
}

# #* s3 bucket
# resource "aws_s3_bucket" "jazz_s3_cloudtrail" {
#   bucket        = "jazz-s3-forCICD-"
#   force_destroy = true
# }