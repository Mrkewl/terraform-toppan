resource "aws_subnet" "alb_public_subnet" {
  vpc_id                  = aws_vpc.jazz_capstone.id
  cidr_block              = var.alb_public_subnet
  map_public_ip_on_launch = true
  availability_zone = var.dmz_public_subnet_availability_zone
  tags = {
    "Name" = var.dmz_public_subnet_name
  }

}

