resource "aws_key_pair" "jazz" {
  key_name   = "jazz"
  public_key = file(var.ssh_public_key)

}



#* fleet_app
resource "aws_instance" "fleet_app" {
  ami                         = "ami-090fa75af13c156b4"
  count                       = 3
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.serverfleet_private_subnet.id
  vpc_security_group_ids      = [aws_security_group.fleet_private_sec_grp.id]
  associate_public_ip_address = false
  key_name                    = aws_key_pair.jazz.key_name
  user_data                   = file("./shell.sh")
  tags = {
    "Name" = "Jazz-fleet"
  }
}
