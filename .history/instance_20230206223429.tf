resource "aws_key_pair" "jazz" {
  key_name   = "jazz"
  public_key = file(var.ssh_public_key)

}



#* Web app
resource "aws_instance" "web_app" {
  ami                         = "ami-090fa75af13c156b4"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front_end_private_subnet.id
  vpc_security_group_ids      = [aws_security_group.frontend_private_sec_grp.id]
  associate_public_ip_address = false
  key_name                    = aws_key_pair.jazz.key_name
  user_data                   = file("./web-app-entrypoint-2.sh")
  tags = {
    "Name" = "Jazz-web-app"
  }
}
