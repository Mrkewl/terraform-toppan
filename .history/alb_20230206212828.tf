#* Create an Application Load Balancer
resource "aws_lb" "jazz_alb" {
  name               = "jazz-webapp-alb"
  internal           = false
  load_balancer_type = "application"
security_groups = [aws_security_group.jazz_elastic_lb_sec_grp.id]

subnet_mapping {
  subnet_id = aws_subnet.front_end_private_subnet.id
}

subnet_mapping {
  subnet_id = aws_subnet.dmz_public_subnet.id
}

 enable_deletion_protection = false
  tags = {
    Name = "jazz_alb"
  }
}