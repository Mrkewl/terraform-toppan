Further improvements:
1. Application Loadbalancer has to be inbetween two subnet, the architecture might need improvements.
2. Having a bastion host might be useful for debugging
3. Instead of having two storage of repository, one in gitlab and one in s3, perhaps we can just use lamba to call gitlab and update the server fleet.
4. Containerization like docker might be helpful to ensure consistency in running environment. ECR can be used for the cicd pipeline
